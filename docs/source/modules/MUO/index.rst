MUO
===

Installation
------------

``git clone https://gitlab.cern.ch/cms-phys-ciemat/muo-corrections.git Corrections/MUO``

RDFModules
----------

.. toctree::
    :maxdepth: 3

    muon 
    muongem 
